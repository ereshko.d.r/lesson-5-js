/*
Описание в README.md

*/

// import NotEmptyFieldError from "./exception.js";

class NotEmptyFieldError extends Error {
    constructor(symbol, numOfField) {
        const message = `You can't put "${symbol}" on filed "${numOfField}"`
        super(message);
        this.name = "NotEmptyFieldError";
    }
}

class TicTacToeLogic {
    #field = [
        null, null, null,
        null, null, null,
        null, null, null,
    ];
    #emptyFields = 9;

    #checkIsEmptyField(numOfField) {
        return this.#field[numOfField] === null;
    }

    #putElement(symbol, numOfField) {
        if (!this.#checkIsEmptyField(numOfField)) {
            return false;
        }
        this.#field[numOfField] = symbol;
        this.#emptyFields--;
        return true;
    }

    #compareCount(symbolX, symbolO) {
        if (symbolX === 3) {
            return 'x'
        }
        if (symbolO === 3) {
            return 'o'
        }
        return false
    }

    #checkHorisontalCombinations() {
        let symbolX = 0;
        let symbolO = 0;
        let resultCompareCount;
        for (let i = 0; i <= 8; i++) {
            if (i % 3 === 0) {
                symbolX = 0;
                symbolO = 0;
            }
            switch (this.#field[i]) {
                case 'x':
                    symbolX++;
                    break;
                case 'o':
                    symbolO++;
                    break;
            }
            resultCompareCount = this.#compareCount(symbolX, symbolO)
            if (resultCompareCount) {
                break;
            }
        }
        
        return resultCompareCount;
    }

    #checkVerticalCombinations() {
        let symbolX = 0;
        let symbolO = 0;
        let resultCompareCount;
        for (let i = 0; i <= 2; i++) {
            for (let k = i; k <= i + 6; k+=3) {
                switch (this.#field[k]) {
                    case 'x':
                        symbolX++;
                        break;
                    case 'o':
                        symbolO++;
                        break;
                }
            }
            resultCompareCount = this.#compareCount(symbolX, symbolO)
            if (resultCompareCount) {
                break;
            }
            symbolX = 0;
            symbolO = 0;
        }
        return resultCompareCount;
    }

    #checkDiagonalCombitations() {
        let symbolX = 0;
        let symbolO = 0;
        let resultCompareCount;
        for (let i = 0; i <= 8; i+=4) {
            switch (this.#field[i]) {
                case 'x':
                    symbolX++;
                    break;
                case 'o':
                    symbolO++;
                    break;
            }
        }
        resultCompareCount = this.#compareCount(symbolX, symbolO)
        if (resultCompareCount) {
            return resultCompareCount;
        }
        symbolX = 0;
        symbolO = 0;
        for (let i = 2; i <= 6; i+=2) {
            switch (this.#field[i]) {
                case 'x':
                    symbolX++;
                    break;
                case 'o':
                    symbolO++;
                    break;
            }
        }
        return this.#compareCount(symbolX, symbolO);
    }

    #checkRules() {
        let resultCheckVerticalCombinations = this.#checkVerticalCombinations();
        let resultChecklHorisontalCombinations = this.#checkHorisontalCombinations();
        let resultCheckDiagonalCombinations = this.#checkDiagonalCombitations();
        if (resultCheckVerticalCombinations) {
            return resultCheckVerticalCombinations;
        }
        else if (resultChecklHorisontalCombinations) {
            return resultChecklHorisontalCombinations;
        }
        else if (resultCheckDiagonalCombinations) {
            return resultCheckDiagonalCombinations;
        }
        else if (this.#emptyFields === 0) {
            return 'draw';
        }
        return false;
    }

    #viewFieldAsText() {
        const field = this.#field;
        let message = '\n';
        for (let i = 0; i < 9; i++) {
            if (i !== 0 && i % 3 === 0) {
                message += '\n';
            }
            message += `${field[i]}\t`;
        }
        return `${message}\n`;
    }

    move(symbol, numOfField) {
        let resultCheckRules;
        if (!this.#putElement(symbol, numOfField)) {
            throw new NotEmptyFieldError(symbol, numOfField)
        }
        resultCheckRules = this.#checkRules();
        if (resultCheckRules) {
            return resultCheckRules;
        }
    }
    
    game(symbol, numOfField, view = false) {
        if (this.#checkRules()){
            return console.log('\nThe game was over\n');
        }
        try {
            const event = this.move(symbol, numOfField);
            if (view) {
                console.log(this.#viewFieldAsText());
            }
            switch (event) {
                case "x":
                    console.log("Symbol 'x' won");
                    break;
                case "o":
                    console.log("Symbol 'o' won");
                    break;
                case "draw":
                    console.log("Draw! Try again");
                    break;
            }
        }
        catch (error) {
            if (error instanceof NotEmptyFieldError) {
                console.log(error.message);
            }
        }
    }

    restart() {
        this.#field = [
            null, null, null,
            null, null, null,
            null, null, null,
        ];
        this.#emptyFields = 9;
    }
}

/*

Autotests

*/

const X = 'x';
const O = 'o';

function test1() {
    const game = new TicTacToeLogic();
    game.game(X, 0);
    game.game(O, 2);
    game.game(X, 3);
    game.game(O, 7);
    game.game(X, 6);
    game.game(O, 4, true);
}

function test2() {
    const game = new TicTacToeLogic();
    game.game(X, 0);
    game.game(O, 3);
    game.game(X, 2);
    game.game(O, 4);
    game.game(X, 6);
    game.game(O, 5, true);
}

function test3() {
    const game = new TicTacToeLogic();
    game.game(X, 0);
    game.game(O, 2);
    game.game(X, 4);
    game.game(O, 3);
    game.game(X, 8, true);
}

function test4() {
    const game = new TicTacToeLogic();
    game.game(X, 0);
    game.game(O, 2);
    game.game(X, 1);
    game.game(O, 3);
    game.game(X, 4);
    game.game(O, 6);
    game.game(X, 5);
    game.game(O, 7);
    game.game(X, 8, true);
}

function test5() {
    const game = new TicTacToeLogic();
    game.game(X, 1);
    game.game(O, 1);
}

test1(); // Ожидается победа "x" по вертикали;
test2(); // Ожидается победа "o" по горизонтали;
test3(); // Ожидается победа "x" по диагонали;
test4(); // Ожидается ничья;
test5(); // Ожидается обработка ошибки NotEmptyFieldError;
